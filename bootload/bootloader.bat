rem install bootloader w/ 8s watchdog
rem set mcu=m328pb
set mcu=m328pb
echo flashing bootloader 
avrdude -c USBasp -B0.5 -p %mcu% -Ulfuse:w:0xFF:m -Uhfuse:w:0xD6:m -Uefuse:w:0xf4:m -Ulock:w:0x3F:m
avrdude -c USBasp -B0.5 -p %mcu% -Uflash:w:optiboot_atmega328_wd8s.hex
pause

